package com.example.ison.motorcyclesetup.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ison.motorcyclesetup.Adapters.RecyclerViewAdapter;
import com.example.ison.motorcyclesetup.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<Integer> mImages = new ArrayList<>();
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    Button btn_handle_bar,btn_pipe, btn_fender, btn_seat, btn_tank, btn_visor, btn_tires ;
    ImageView imgView, imgViewHandleBar, imgViewPipe, imgViewFender, imgViewSeat, imgViewTank,imgViewVisor;
    TextView handle_bar_price, pipe_price, fender_price, seat_price, tank_price, visor_price, total_price;
    int handle_bar_amount = 0, pipe_amount = 0, fender_amount = 0, seat_amount = 0, tank_amount = 0, visor_amount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        initUI();
        getHandleBar();

    }

    public void initUI(){
        btn_handle_bar = (Button) findViewById(R.id.btn_handle_bar);
        btn_handle_bar.setOnClickListener(this);
        btn_fender = (Button) findViewById(R.id.btn_fender);
        btn_fender.setOnClickListener(this);
        btn_pipe = (Button) findViewById(R.id.btn_pipe);
        btn_pipe.setOnClickListener(this);
        btn_seat = (Button) findViewById(R.id.btn_seat);
        btn_seat.setOnClickListener(this);
        btn_tank = (Button) findViewById(R.id.btn_tank);
        btn_tank.setOnClickListener(this);
        btn_visor = (Button) findViewById(R.id.btn_visor);
        btn_visor.setOnClickListener(this);
        imgView = (ImageView) findViewById(R.id.imgSkeleton);
        imgViewHandleBar = (ImageView) findViewById(R.id.imgHandleBar);
        imgViewPipe = (ImageView) findViewById(R.id.imgPipe);
        imgViewFender = (ImageView) findViewById(R.id.imgFender);
        imgViewSeat = (ImageView) findViewById(R.id.imgSeat);
        imgViewTank = (ImageView) findViewById(R.id.imgTank);
        imgViewVisor = (ImageView) findViewById(R.id.imgVisor);
        handle_bar_price = (TextView) findViewById(R.id.handle_bar_price);
        pipe_price = (TextView) findViewById(R.id.pipe_price);
        fender_price = (TextView) findViewById(R.id.fender_price);
        seat_price = (TextView) findViewById(R.id.seat_price);
        tank_price = (TextView) findViewById(R.id.tank_price);
        visor_price = (TextView) findViewById(R.id.visor_price);
        total_price = (TextView) findViewById(R.id.total_price);
    }


    private void getHandleBar(){
        Log.d(TAG, "getImages: ");

        mImages.add(R.drawable.handle_bar_1);
        mNames.add("Handle Bar 1");

        mImages.add(R.drawable.handle_bar_2);
        mNames.add("Handle Bar 2");

        initRecyclerView();
    }

    private void getPipe(){
        Log.d(TAG, "getPipe: ");

        mImages.add(R.drawable.pipe_1);
        mNames.add("Pipe 1");

        mImages.add(R.drawable.pipe_2);
        mNames.add("Pipe 2");

        mImages.add(R.drawable.pipe_3);
        mNames.add("Pipe 3");

        initRecyclerView();
    }

    private void getFender(){
        Log.d(TAG, "getFender: ");

        mImages.add(R.drawable.rear_fender_1);
        mNames.add("Fender 1");

        mImages.add(R.drawable.rear_fender_2);
        mNames.add("Fender 2");

        initRecyclerView();
    }


    private void getSeat(){
        Log.d(TAG, "getSeat: ");

        mImages.add(R.drawable.seat_1);
        mNames.add("Seat 1");

        mImages.add(R.drawable.seat_2);
        mNames.add("Seat 2");

        initRecyclerView();
    }

    private void getTank(){
        Log.d(TAG, "getSeat: ");

        mImages.add(R.drawable.tank_1);
        mNames.add("Tank 1");

        mImages.add(R.drawable.tank_2);
        mNames.add("Tank 2");

        initRecyclerView();
    }

    private void getVisor(){
        Log.d(TAG, "getVisor: ");

        mImages.add(R.drawable.visor_1);
        mNames.add("Visor 1");

        mImages.add(R.drawable.visor_2);
        mNames.add("Visor 2");

        mImages.add(R.drawable.visor_3);
        mNames.add("Visor 3");

        initRecyclerView();
    }



    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: ");

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter(this, mNames, mImages);
        recyclerView.setAdapter(adapter);
                
    }

    public void clearItems(){
        mNames.clear();
        mImages.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_handle_bar:
                clearItems();
                getHandleBar();
                break;

            case R.id.btn_pipe:
                clearItems();
                getPipe();
                break;

            case R.id.btn_fender:
                clearItems();
                getFender();
                break;

            case R.id.btn_seat:
                clearItems();
                getSeat();
                break;

            case R.id.btn_tank:
                clearItems();
                getTank();
                break;

            case R.id.btn_visor:
                clearItems();
                getVisor();
                break;

            default:
                break;
        }
    }

    public void updateImgViewHandleBar(int imageID, int mPrice){
        imgViewHandleBar.setBackgroundResource(imageID);
        handle_bar_price.setText("Handle Bar : " + String.valueOf(mPrice));
        handle_bar_amount = mPrice;
        setTotal_price();
    }

    public void updateImgViewPipe(int imageID, int mPrice){
        imgViewPipe.setBackgroundResource(imageID);
        pipe_price.setText("Pipe : " +String.valueOf(mPrice));
        pipe_amount = mPrice;
        setTotal_price();
    }

    public void updateImgViewFender(int imageID, int mPrice){
        imgViewFender.setBackgroundResource(imageID);
        fender_price.setText("Fender : " + String.valueOf(mPrice));
        fender_amount = mPrice;
        setTotal_price();
    }

    public void updateImgViewSeat(int imageID, int mPrice){
        imgViewSeat.setBackgroundResource(imageID);
        seat_price.setText("Seat : " + String.valueOf(mPrice));
        seat_amount = mPrice;
        setTotal_price();
    }

    public void updateImgViewTank(int imageID, int mPrice){
        imgViewTank.setBackgroundResource(imageID);
        tank_price.setText("Tank : " + String.valueOf(mPrice));
        tank_amount = mPrice;
        setTotal_price();
    }

    public void updateImgViewVisor(int imageID, int mPrice){
        imgViewVisor.setBackgroundResource(imageID);
        visor_price.setText("Visor : " + String.valueOf(mPrice));
        visor_amount = mPrice;
        setTotal_price();
    }

    public void setTotal_price(){
        total_price.setText("Total : " + String.valueOf(handle_bar_amount + pipe_amount + fender_amount +seat_amount + tank_amount + visor_amount));
    }
}
