package com.example.ison.motorcyclesetup.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.ison.motorcyclesetup.R;

import gr.net.maroulis.library.EasySplashScreen;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        EasySplashScreen config = new EasySplashScreen((Splash.this))
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(4000)
                .withLogo(R.drawable.splash);
        View view = config.create();
        setContentView(view);
    }
}
