package com.example.ison.motorcyclesetup.Adapters;

import android.content.Context;
import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ison.motorcyclesetup.Activities.MainActivity;
import com.example.ison.motorcyclesetup.R;

import java.util.ArrayList;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<Integer> mImages = new ArrayList<>();
    private int mPrice = 0;
    private Context mContext;

    public RecyclerViewAdapter(Context mContext, ArrayList<String> mNames, ArrayList<Integer> mImages) {
        this.mNames = mNames;
        this.mImages = mImages;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

//        Glide.with(mContext)
//                .asBitmap()
//                .load(mImages)
//                .into(holder.image);



        holder.image.setImageResource(mImages.get(position));
        holder.name.setText(mNames.get(position));

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: " + mNames.get(position));
                Toast.makeText(mContext, mNames.get(position), Toast.LENGTH_SHORT ).show();

                switch (mNames.get(position)){
                    case "Handle Bar 1":
                        ((MainActivity)mContext).updateImgViewHandleBar(R.drawable.handle_bar_1_part, 1193);
                        break;

                    case "Handle Bar 2" :
                        ((MainActivity)mContext).updateImgViewHandleBar(R.drawable.handler_bar_2_part, 1740);
                        break;

                    case "Pipe 1" :
                        ((MainActivity)mContext).updateImgViewPipe(R.drawable.pipe_1_part, 1819);
                        break;

                    case "Pipe 2" :
                        ((MainActivity)mContext).updateImgViewPipe(R.drawable.pipe_2_part, 1730);
                        break;

                    case "Pipe 3" :
                        ((MainActivity)mContext).updateImgViewPipe(R.drawable.pipe_3_part, 2061);
                        break;

                    case "Fender 1" :
                        ((MainActivity)mContext).updateImgViewFender(R.drawable.fender_1_part, 1200);
                        break;

                    case "Fender 2" :
                        ((MainActivity)mContext).updateImgViewFender(R.drawable.fender_2_part, 1693);
                        break;

                    case "Seat 1" :
                        ((MainActivity)mContext).updateImgViewSeat(R.drawable.seat_1_part, 969);
                        break;

                    case "Seat 2" :
                        ((MainActivity)mContext).updateImgViewSeat(R.drawable.seat_2_part, 479);

                        break;

                    case "Tank 1" :
                        ((MainActivity)mContext).updateImgViewTank(R.drawable.tank_1_part, 4079);

                        break;

                    case "Tank 2" :
                        ((MainActivity)mContext).updateImgViewTank(R.drawable.tank_2_part, 3845);
                        break;

                    case "Visor 1" :
                        ((MainActivity)mContext).updateImgViewVisor(R.drawable.visor_1_part,950);
                        break;

                    case "Visor 2" :
                        ((MainActivity)mContext).updateImgViewVisor(R.drawable.visor_2_part,1070);
                        break;

                    case "Visor 3" :
                        ((MainActivity)mContext).updateImgViewVisor(R.drawable.visor_3_part, 1198);
                        break;

                    default:
                         break;
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return mNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            name = itemView.findViewById(R.id.name);
        }
    }

}
